#!/usr/bin/env python

from setuptools import setup


requirements = [
    'praw==3.6.2',
    'Whoosh==2.7.2',
]

tests_requirements = ['tox']

setup(
    name='reddit-saved-search',
    version='2.0.0',
    description='Command line utility for searching through your "saved links" on reddit, using reddit\'s API',
    author='Arjen Verstoep',
    author_email='arjen.verstoep@gmail.com',
    license='BSD',
    packages=['reddit_saved_search'],
    install_requires=requirements,
    extras_require={
        'test': tests_requirements,
    },
    entry_points={
        'console_scripts': [
            'reddit-saved-search=reddit_saved_search.__main__:main',
        ],
    },
)
