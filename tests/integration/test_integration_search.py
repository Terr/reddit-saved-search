import os

from unittest.mock import patch
from pathlib import Path

from whoosh.index import create_in
from whoosh import fields

from reddit_saved_search import search


class TestWhooshSearchClientFactory:
    def test_search_index_is_created_if_it_does_not_exist(self, tmpdir: Path):
        search_index_path = tmpdir / 'test'

        result = search.WhooshSearchClientFactory.get_instance(search_index_path)

        assert os.path.exists(str(search_index_path))
        assert isinstance(result, search.WhooshSearchClient)

    def test_existing_search_index_is_loaded(self, tmpdir: Path):
        search_index_path = tmpdir / 'test'
        os.makedirs(str(search_index_path))
        create_in(str(search_index_path), fields.Schema(test=fields.TEXT()))

        with patch.object(search, 'open_dir', wraps=search.open_dir) as open_dir_spy:
            result = search.WhooshSearchClientFactory.get_instance(search_index_path)

        open_dir_spy.assert_called_once_with(str(search_index_path))
        assert isinstance(result, search.WhooshSearchClient)
