import pytest
import shutil
import tempfile

from pathlib import Path


@pytest.fixture
def tmpdir():
    """Creates a temporary directory, passes it as a Path to the test function,
    and then cleans it up afterwards.
    """

    tmpdir = tempfile.mkdtemp()
    yield Path(tmpdir)
    shutil.rmtree(tmpdir)
