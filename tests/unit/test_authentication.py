from copy import copy
from http.server import HTTPServer
from io import BytesIO
from unittest.mock import Mock

from reddit_saved_search import authentication


class TestOAuthHTTPCallbackHandler:
    def setup_method(self):
        # Check and preserve expected initial state, to restore it after the test
        self.original_state = copy(authentication.OAuthHTTPCallbackHandler.last_oauth_callback_params)

        authentication.OAuthHTTPCallbackHandler.last_oauth_callback_params = {'code': None}

    def teardown_method(self):
        # Restore initial state
        authentication.OAuthHTTPCallbackHandler.last_oauth_callback_params = self.original_state

    def test_parses_access_code_from_querystring(self):
        expected_code = 'ABC123'

        request_url = b'/auth/?code=%b' % expected_code.encode('ASCII')
        request = Mock()
        request.makefile.return_value = BytesIO(b'GET %b' % request_url)

        server = Mock(spec=HTTPServer)

        authentication.OAuthHTTPCallbackHandler(request, ('127.0.0.1', 9000), server)

        assert expected_code == authentication.OAuthHTTPCallbackHandler.last_oauth_callback_params['code']
        server.shutdown.assert_called_once_with()

    def test_responds_with_bad_request_if_access_code_parameter_is_missing(self):
        request_url = b'/'
        request = Mock()
        request.makefile.return_value = BytesIO(b'GET %b' % request_url)

        server = Mock(spec=HTTPServer)

        authentication.OAuthHTTPCallbackHandler(request, ('127.0.0.1', 9000), server)

        assert authentication.OAuthHTTPCallbackHandler.last_oauth_callback_params['code'] is None
        # TODO Figure out how to access the status code and HTML returned by
        # the callback handler
