import os
import pickle
import pytest

from unittest.mock import Mock, patch, mock_open

from reddit_saved_search import config


platform_cache_dirs = [
    ('Darwin', '/Users/test-user', '/Users/test-user/Library/Caches/reddit-saved-search'),
    ('posix', '/home/test-user', '/home/test-user/.cache/reddit-saved-search'),
]


class TestConfig:
    def setup_method(self):
        self.config = config.Config()

    def test_get_client_secret_reads_secret_file_when_env_variable_is_not_set(self):
        secret = 'test-data'
        mock_secret_file = mock_open(read_data=secret)

        with patch.object(config.os, 'environ', {}), \
                patch.object(config, 'open', mock_secret_file):
            result = self.config.get_client_secret()

        assert mock_secret_file.called, 'Secret file was not opened'
        mock_secret_file.assert_called_once_with(os.path.join(os.getcwd(), 'secret.txt'), 'r')

        mock_secret_file_handle = mock_secret_file()
        assert mock_secret_file_handle.readline.called, 'Secret file was not read'

        assert secret == result, 'Returned secret is different than expected'

    def test_get_client_secret_gets_code_from_env_variable_when_present(self):
        mock_secret_file = mock_open(read_data='test_data')
        secret = 'test-secret'

        with patch.object(config.os, 'environ', {'RSS_CLIENT_SECRET': secret}), \
                patch.object(config, 'open', mock_secret_file):
            result = self.config.get_client_secret()

        assert not mock_secret_file.called
        assert secret == result, 'Returned secret is different than expected'

    def test_get_client_secret_returns_none_when_both_secret_file_and_env_variable_do_not_exist(self):
        mock_secret_file = Mock(side_effect=FileNotFoundError)

        with patch.object(config, 'open', mock_secret_file):
            result = self.config.get_client_secret()

        assert result is None

    def test_get_access_credentials_reads_from_file_if_it_exists(self):
        access_credentials = {'user': 'UnitTest'}
        mock_identity_storage = mock_open(read_data=pickle.dumps(access_credentials))

        with patch.object(config, 'open', mock_identity_storage), \
                patch.object(config, 'os') as mock_os:
            mock_os.path.expanduser.return_value = '/test'
            result = self.config.get_access_credentials()

        mock_identity_storage.assert_called_once_with('/test/auth.pickle', 'rb')
        assert access_credentials == result

    def test_get_access_credentials_returns_none_when_storage_file_does_not_exist(self):
        mock_open_file_not_found = Mock(side_effect=FileNotFoundError)

        with patch.object(config, 'open', mock_open_file_not_found):
            result = self.config.get_access_credentials()

        assert result is None

    def test_get_access_credentials_returns_none_invalid_data_is_read_from_file(self):
        mock_pickle = Mock()
        mock_pickle.loads.side_effect = EOFError
        mock_identity_storage = mock_open(read_data='')

        with patch.object(config, 'pickle', mock_pickle), \
                patch.object(config, 'open', mock_identity_storage):
            result = self.config.get_access_credentials()

        assert mock_identity_storage.called
        assert result is None

    def test_store_access_credentials_writes_data_to_file(self):
        access_credentials = {'user': 'UnitTest'}
        mock_identity_storage = mock_open()
        mock_pickle = Mock()
        mock_pickle.dumps.return_value = 'pickled_data'

        with patch.object(config, 'pickle', mock_pickle), \
                patch.object(config, 'open', mock_identity_storage), \
                patch.object(config, 'os') as mock_os:
            mock_os.path.expanduser.return_value = '/test'
            self.config.store_access_credentials(access_credentials)

        # mock_os.makedirs.assert_called_once_with('/test', exist_ok=True, mode=0o770)
        mock_identity_storage.assert_called_once_with('/test/auth.pickle', 'wb')
        mock_identity_storage_handle = mock_identity_storage()
        mock_identity_storage_handle.write.assert_called_once_with('pickled_data')
        mock_pickle.dumps.assert_called_once_with(access_credentials)

    @pytest.mark.parametrize('platform,homedir,expected', platform_cache_dirs)
    def test_correct_cache_directory_is_created_depending_on_platform(self, platform: str, homedir: str,
                                                                      expected: str):
        with patch.object(config, 'open'), \
                patch.object(config, 'os') as mock_os, \
                patch.object(config, 'platform') as mock_platform, \
                patch.dict(os.environ, {'HOME': homedir}):
            mock_os.path = os.path
            mock_platform.system.return_value = platform
            self.config.store_access_credentials({})

        mock_os.makedirs.assert_called_once_with(
            os.path.expanduser(expected),
            mode=0o770,
            exist_ok=True)
