from collections import namedtuple
from unittest.mock import Mock, MagicMock, call

from whoosh.index import Index
from whoosh.query import Term

from reddit_saved_search import search


Submission = namedtuple('Submission', ['title', 'url', 'fullname'])


class TestWhooshSearchClient:
    def setup_method(self):
        self.whoosh_search_index = MagicMock(spec=Index, schema='S')
        self.search_client = search.WhooshSearchClient(self.whoosh_search_index)

    def test_updates_search_index_with_new_submissions(self):
        submissions = [
            Submission('Title 1', 'URL1', 't3_1'),
            Submission('Title 2', 'URL2', 't3_2'),
            Submission('Title 3', 'URL3', 't3_3'),
        ]

        index_searcher = Mock()
        index_searcher.search.return_value = []
        self.whoosh_search_index.searcher.return_value.__enter__.return_value = index_searcher

        index_writer = Mock()
        self.whoosh_search_index.writer.return_value = index_writer

        self.search_client.add_submissions(submissions)

        self.whoosh_search_index.writer.assert_called_once_with()
        index_writer.add_document.assert_has_calls([
            call(title=submissions[0].title, url=submissions[0].url, fullname=submissions[0].fullname),
            call(title=submissions[1].title, url=submissions[1].url, fullname=submissions[1].fullname),
            call(title=submissions[2].title, url=submissions[2].url, fullname=submissions[2].fullname),
        ])
        index_writer.commit.assert_called_once_with()

    def test_does_nothing_when_given_no_submissions_to_add(self):
        self.search_client.add_submissions([])

        self.whoosh_search_index.writer.assert_not_called()

    def test_stops_with_updating_index_when_encountering_submission_that_is_already_indexed(self):
        submissions = [
            Submission('Title 1', 'URL1', 't3_1'),
            Submission('Title 2', 'URL2', 't3_2'),
            Submission('Title 3', 'URL3', 't3_3'),
        ]

        index_searcher = Mock()
        index_searcher.search.side_effect = [[], [True]]  # 2nd submission will be seen as 'already in index'
        self.whoosh_search_index.searcher.return_value.__enter__.return_value = index_searcher

        index_writer = Mock()
        self.whoosh_search_index.writer.return_value = index_writer

        self.search_client.add_submissions(submissions)

        assert 2 == index_searcher.search.call_count
        index_writer.add_document.assert_has_calls([
            call(title=submissions[0].title, url=submissions[0].url, fullname=submissions[0].fullname),
        ])

    def test_searching_yields_results(self):
        query = 'test'

        index_searcher = Mock()
        index_searcher.search.return_value = ['A', 'B']
        self.whoosh_search_index.searcher.return_value.__enter__.return_value = index_searcher

        results = self.search_client.search(query)

        assert 'A' == next(results)
        assert 'B' == next(results)
        index_searcher.search.assert_called_with(Term('title', query))
