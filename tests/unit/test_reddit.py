from unittest.mock import Mock, call
from collections import namedtuple

from praw import Reddit

from reddit_saved_search import reddit


CommentPost = namedtuple('CommentPost', ['fullname', 'submission'])
Post = namedtuple('Post', ['fullname'])


class TestPrawRedditClient:
    def setup_method(self):
        self.praw_reddit = Mock(spec=Reddit)
        self.praw_reddit.user = Mock()
        self.praw_client = reddit.PrawRedditClient(
            self.praw_reddit,
            client_id='ID1',
            client_secret='ABCDEFGHIJKLMN',
            callback_url='http://localhost:4242/auth_callback')

    def test_refreshing_access_credentials_makes_call_to_api(self):
        access_credentials = {'access': 'granted'}
        refresh_token = 'abracadabra'
        self.praw_reddit.refresh_access_information.return_value = access_credentials

        result = self.praw_client.refresh_access_information(refresh_token)

        self.praw_reddit.refresh_access_information.assert_called_once_with(refresh_token)
        self.praw_reddit.set_access_credentials.assert_called_once_with(**access_credentials)
        assert access_credentials == result

    def test_iterate_reddit_saved_submittions_paginates_through_many_results(self):
        posts_page_1 = self._get_posts(100)
        posts_page_1[99] = self._get_comment_post()
        posts_page_2 = self._get_posts(50)
        self.praw_reddit.user.get_saved.side_effect = [
            posts_page_1,
            posts_page_2,
            []
        ]

        expected_params_page_1 = {'limit': 100}
        expected_params_page_2 = {'limit': 100, 'count': 100, 'after': 'comment1'}
        expected_params_page_3 = {'limit': 100, 'count': 150, 'after': 'fullname50'}

        result = list(self.praw_client.get_saved_submissions())

        self.praw_reddit.user.get_saved.assert_has_calls([
            call(params=expected_params_page_1),
            call(params=expected_params_page_2),
            call(params=expected_params_page_3),
        ])

        assert 150 == len(result)
        assert posts_page_1[99].submission == result[99]
        assert posts_page_2[49] == result[149]

    def _get_posts(self, number_of_posts):
        """Returns the requested amount of PRAW Post-like data object."""
        output = []

        for x in range(1, number_of_posts + 1):
            output.append(Post(fullname='fullname{}'.format(x)))

        return output

    def _get_comment_post(self):
        """Returns a PRAW CommentPost-like data object."""
        return CommentPost(
            fullname='comment1',
            submission=Post(fullname='post1'))
