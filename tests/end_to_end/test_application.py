import time

from contextlib import redirect_stdout
from collections import namedtuple
from http.client import HTTPConnection
from io import StringIO
from pathlib import Path
from threading import Thread
from unittest.mock import patch, Mock
from urllib.parse import urlparse

from praw import Reddit

from reddit_saved_search import reddit, search
from reddit_saved_search.application import Application
from reddit_saved_search.config import Config


Submission = namedtuple('Submission', ['title', 'url', 'fullname'])


class AuthCallbackThread(Thread):
    """Makes HTTP requests to the given callback URL until it gets a response."""

    def __init__(self, callback_url: str) -> None:
        super().__init__()

        self._callback_url = callback_url

    def run(self) -> None:
        parsed_callback_url = urlparse(self._callback_url)
        max_number_of_attempts = 10
        for _ in range(0, max_number_of_attempts):
            http_conn = HTTPConnection(*parsed_callback_url.netloc.split(':'))
            try:
                http_conn.request('GET', parsed_callback_url.path + '?code=ABC123')
                break
            except ConnectionRefusedError:
                time.sleep(0.2)
                continue

        try:
            http_conn.getresponse()
        finally:
            http_conn.close()


@patch('builtins.input')
def test_user_is_able_to_search_through_saved_submissions(keyboard_input, tmpdir: Path):
    config = Config()
    praw_reddit = Mock(spec=Reddit)
    praw_reddit.get_access_information.return_value = {}
    praw_reddit.refresh_access_information.return_value = {}
    praw_reddit.user = Mock()
    praw_reddit.user.get_saved.side_effect = [
        (Submission('This is a test post', 'URL1', 't3_1'),),
        tuple(),
    ]
    keyboard_input.side_effect = ['test', EOFError]

    reddit_client = reddit.PrawRedditClient(praw_reddit,
                                            config.get_client_id(),
                                            'secret',
                                            config.get_auth_callback_url())

    search_client = search.WhooshSearchClientFactory.get_instance(tmpdir / 'index')

    callback_thread = AuthCallbackThread(config.get_auth_callback_url())
    callback_thread.setDaemon(True)
    callback_thread.start()

    output = StringIO()
    with redirect_stdout(output):
        application = Application(config, reddit_client, search_client)
        application.run()

    assert 'This is a test post' in output.getvalue()
