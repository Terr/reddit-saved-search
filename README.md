# Reddit Saved Search

## Requirements

* Python >= 3.5

## Running tests

In a virtual environment (e.g. virtualenv, Docker):

```
pip install -e '.[test]'
tox
```

### Running tests for individual environments via Docker

To run the tests using, for example, Python 3.6:

```
docker run --rm -v `pwd`:/src python:3.6 bash -c "cd /src && pip install -e '.[test]' && tox -e py36"
```

## Development history

Up to v1.0.1, this project served as an experiment for me to make something procedural again: no classes (unless they come from external libraries), only pure functions, and without resorting to global variables to keep some state. I was curious to see how it would affect testing, because the last time I programmed something bigger procedurally (pre-2008) I did not know about unit testing and mocking.

My conclusion? With Python, it's actually quite doable. When testing more complex functions (such as the top-level functions in the project), by using unittest.mock's patch feature it's possible to override dependent functions almost like you would inject dependencies into a class. When you divide your functions up into packages by responsibility (like you would do with classes) and consider a package as the 'unit to test', mocking out its imported packages gives you the familiar ability to influence return values and do call assertions.

In some ways testing is easier in a procedural program than in (complex) object-oriented programs. Because functions don't have state, unless you shoot yourself in the foot by using globals, the flow from input to output is very easy to follow.

The biggest challenge for me was to keep the number of parameters per function manageable. In the end, I felt that some of the top-level functions needed too many arguments, most of which were only needed to pass on directly to other functions. Maybe it's something that can be avoided, maybe something for a next experiment.

After v1.0.1 was done, I refactored the project and split its functionality into classes.
