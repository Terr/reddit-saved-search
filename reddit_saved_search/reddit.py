from praw import Reddit

from typing import Generator


class PrawRedditClient:
    """Adapter for the PRAW reddit client."""

    def __init__(self, praw: Reddit, client_id: str, client_secret: str, callback_url: str) -> None:
        self._praw = praw
        self._set_oauth_info(client_id, client_secret, callback_url)

    def _set_oauth_info(self, client_id: str,  client_secret: str, callback_url: str):
        self._praw.set_oauth_app_info(
            client_id=client_id,
            client_secret=client_secret,
            redirect_uri=callback_url
        )

    def get_authorize_url(self) -> str:
        """Returns the URL to the OAuth authorization page on reddit.com."""

        return self._praw.get_authorize_url('uniqueKey', ['identity', 'history'], True)

    def refresh_access_information(self, refresh_token: str) -> dict:
        access_credentials = self._praw.refresh_access_information(refresh_token)
        self._praw.set_access_credentials(**access_credentials)

        return access_credentials

    def get_access_information(self, access_code: str) -> dict:
        return self._praw.get_access_information(access_code)

    def get_saved_submissions(self) -> Generator:
        """Iterates through all of the user's saved submissions on Reddit."""

        index_count = 0
        last_iteration_index_count = 0
        last_submission_fullname = None
        limit_per_page = 100

        while True:
            last_iteration_index_count = index_count

            params = {
                'limit': limit_per_page
            }

            if last_submission_fullname:
                params.update({
                    'after': last_submission_fullname,
                    'count': index_count
                })

            saved_posts = self._praw.user.get_saved(params=params)

            for post in saved_posts:
                # Comments don't have a title, so we need to take it from the post
                # they're placed under
                if hasattr(post, 'submission'):
                    submission = post.submission
                else:
                    submission = post

                last_submission_fullname = post.fullname
                index_count += 1

                yield submission

            if last_iteration_index_count == index_count:
                # No more posts
                break
