from typing import Optional, List


class Analyzer:
    pass

class NgramFilter:
    def __init__(self, minsize: int, maxsize: Optional[int]) -> None: ...

def StandardAnalyzer(expression: str = ..., stoplist: Optional[List[str]] = ..., minsize: int = ..., gaps: bool = ...): ...
