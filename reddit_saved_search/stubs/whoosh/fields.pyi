from typing import Optional, Any

from whoosh.analysis import Analyzer
from whoosh.formats import Format


class Schema:
    def __init__(self, **fields) -> None: ...


class TEXT:
    def __init__(self, analyzer: Optional[Analyzer] = ..., phrase: bool = ..., vector: Optional[Format] = ..., stored: bool = ..., field_boost: float = ...) -> None: ...
