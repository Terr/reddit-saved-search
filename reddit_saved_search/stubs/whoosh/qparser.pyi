from typing import Optional, Union

from whoosh.fields import Schema
from whoosh.query import Query


class QueryParser:
    def __init__(self, default_field: str, schema: Union[Schema, str, None] = ..., conjunction: Query = ..., termclass: Query = ...) -> None: ...

    def parse(self, input: str, normalize: bool = ...) -> Query: ...
