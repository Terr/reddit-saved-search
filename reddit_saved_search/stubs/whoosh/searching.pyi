from typing import Optional

from whoosh.query import Query


class Results:
    pass


class Searcher:
    def search(self, query: Query, limit: int = ..., sortedby: Optional[str] = ..., reverse: bool = ..., minscore: float = ...) -> Results: ...

    def __enter__(self): ...
    def __exit__(self, *args, **kwargs): ...
