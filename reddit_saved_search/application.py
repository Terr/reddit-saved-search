import webbrowser

from praw import Reddit

from reddit_saved_search import search, reddit, terminal, authentication
from reddit_saved_search.config import Config


class Application:
    def __init__(self,
                 config: Config,
                 reddit_client: reddit.PrawRedditClient,
                 search_client: search.WhooshSearchClient) -> None:
        self._config = config
        self._reddit_client = reddit_client
        self._search_client = search_client

    def run(self) -> None:
        self._authenticate()
        # TODO Run in separate thread with callback to UI class for live updates
        self._update_search_client()

        # TODO To UI class
        print('Indexed {:,} submissions'.format(self._search_client.get_submissions_count()))
        self._main_loop()

    def _authenticate(self) -> None:
        access_credentials = self._config.get_access_credentials()

        if access_credentials:
            fresh_access_credentials = self._reddit_client.refresh_access_information(
                access_credentials['refresh_token'])
        else:
            self._open_auth_page_in_webbrowser()
            access_code = authentication.get_oauth_from_http_callback(self._config.get_auth_callback_url())
            fresh_access_credentials = self._reddit_client.get_access_information(access_code)
        self._config.store_access_credentials(fresh_access_credentials)

    def _open_auth_page_in_webbrowser(self) -> None:
        url = self._reddit_client.get_authorize_url()
        webbrowser.open(url)

    def _update_search_client(self) -> None:
        self._search_client.add_submissions(self._reddit_client.get_saved_submissions())

    def _main_loop(self) -> None:
        while True:
            try:
                search_query = input('Search query: ')
                terminal.print_results(self._search_client.search(search_query))
            except EOFError:
                # User cancelled input
                break


def bootstrap_application():
    config = Config()

    reddit_client = reddit.PrawRedditClient(
        praw=Reddit('Reddit Saved Search'),
        client_id=config.get_client_id(),
        client_secret=config.get_client_secret(),
        callback_url=config.get_auth_callback_url())

    search_client = search.WhooshSearchClientFactory.get_instance(config.get_search_index_path())

    application = Application(config, reddit_client, search_client)
    application.run()


if __name__ == '__main__':
    bootstrap_application()
