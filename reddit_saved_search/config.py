import os
import pickle
import platform

from pathlib import Path
from typing import Optional


class Config:
    def get_client_id(self) -> str:
        return 'GtuxQgWciIzWbA'

    def get_client_secret(self) -> str:
        if 'RSS_CLIENT_SECRET' in os.environ:
            return os.environ.get('RSS_CLIENT_SECRET')

        try:
            with open(str(self._get_secret_file_path()), 'r') as secret_file:
                client_secret = secret_file.readline().strip()
            return client_secret
        except FileNotFoundError:
            return None

    def _get_secret_file_path(self) -> Path:
        return self._get_current_dir() / 'secret.txt'

    def _get_current_dir(self) -> Path:
        return Path(os.getcwd())

    def get_access_credentials(self) -> Optional[dict]:
        try:
            with open(str(self._get_access_credentials_storage_path()), 'rb') as storage_file:
                raw_auth_dict = storage_file.read()
                return pickle.loads(raw_auth_dict)
        except (FileNotFoundError, EOFError):
            return None

    def _get_access_credentials_storage_path(self) -> Path:
        return self._get_storage_path() / 'auth.pickle'

    def _get_storage_path(self) -> Path:
        cache_directory = self._get_cache_directory_based_on_platform()
        self._create_cache_directory(cache_directory)

        return cache_directory

    def _create_cache_directory(self, cache_path: Path) -> None:
        """Creates, if not present, and returns the path to the application's cache directory."""

        os.makedirs(str(cache_path), mode=0o770, exist_ok=True)

    def _get_cache_directory_based_on_platform(self) -> Path:
        if platform.system() == 'Darwin':
            return Path(os.path.expanduser('~/Library/Caches/reddit-saved-search'))
        else:
            # Assume POSIX (e.g. Linux)
            return Path(os.path.expanduser('~/.cache/reddit-saved-search'))

    def get_search_index_path(self) -> Path:
        return self._get_storage_path() / 'index'

    def get_auth_callback_url(self) -> str:
        return 'http://localhost:9000/auth/callback'

    def store_access_credentials(self, access_credentials: dict) -> None:
        with open(str(self._get_access_credentials_storage_path()), 'wb') as identity_storage_file:
            identity_storage_file.write(pickle.dumps(access_credentials))
