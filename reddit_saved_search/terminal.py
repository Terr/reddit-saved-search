class Color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def cprint(color, text):
    """Print `text` on the terminal in the selected `color`."""
    print(color + text + Color.END)


def print_results(results):
    print('-' * 40)

    for result in results:
        cprint(Color.BOLD, result['title'])
        cprint(Color.BLUE, result['url'])
        print('-' * 40)
