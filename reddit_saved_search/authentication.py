import threading

from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs


class OAuthHTTPCallbackHandler(BaseHTTPRequestHandler):
    last_oauth_callback_params = {'code': None}  # type: dict

    def do_GET(self):
        parsed_request_url = urlparse(self.path)
        querystring = parse_qs(parsed_request_url.query)
        code = querystring.get('code', [None])[0]

        if code:
            self.send_response(HTTPStatus.OK)
            self.last_oauth_callback_params.update({'code': code})
            body = 'Code: ' + code
        else:
            self.send_response(HTTPStatus.BAD_REQUEST)
            self.last_oauth_callback_params.update({'code': None})
            body = 'Error!'

        self.send_header('Content-Type', 'text/html; charset=utf-8')
        self.send_header('Content-Length', len(body))
        self.end_headers()

        self.wfile.write(body.encode('utf-8'))

        self._shutdown_server()

    def _shutdown_server(self):
        """Shutdown the server after serving the request.

        See http://stackoverflow.com/a/22533929
        """
        thread = threading.Thread(target=self.server.shutdown)
        thread.daemon = True
        thread.start()


def get_oauth_from_http_callback(auth_callback_url: str) -> str:
    parsed_callback_url = urlparse(auth_callback_url)
    address, port = parsed_callback_url.netloc.split(':')

    http_server = HTTPServer((address, int(port)), OAuthHTTPCallbackHandler)
    http_server.handle_request()

    return OAuthHTTPCallbackHandler.last_oauth_callback_params['code']
