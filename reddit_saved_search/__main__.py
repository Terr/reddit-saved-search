from reddit_saved_search.application import bootstrap_application


def main():
    bootstrap_application()


if __name__ == '__main__':
    main()
