import os

from typing import Iterable, Generator
from pathlib import Path

from whoosh.index import Index, create_in, open_dir
from whoosh import fields
from whoosh.analysis import NgramFilter, StandardAnalyzer
from whoosh.qparser import QueryParser

from praw.objects import RedditContentObject


class WhooshSearchClient:
    def __init__(self, whoosh_search_index: Index) -> None:
        self._whoosh_search_index = whoosh_search_index

    def add_submissions(self, submissions: Iterable[RedditContentObject]) -> None:
        """Adds `submissions` to the search index.

        The given list of submissions should always be given in the same (e.g.
        chronological) order. Adding of submissions stops when one is encountered
        that is already in the search index.
        """

        if not submissions:
            return

        writer = self._whoosh_search_index.writer()

        for submission in submissions:
            try:
                next(self.search(submission.fullname, 'fullname'))
            except StopIteration:
                # Submission is not already in the index so we can add it
                writer.add_document(
                    fullname=submission.fullname,
                    title=submission.title,
                    url=submission.url
                )
            else:
                # Submission is already in the index. That means that this and
                # following submissions have already been stored, so we
                # can stop with iterating through these submissions
                break

        writer.commit()

    def search(self, search_query: str, field: str='title') -> Generator:
        with self._whoosh_search_index.searcher() as searcher:
            parser = QueryParser(field, self._whoosh_search_index.schema)
            query = parser.parse(search_query)
            results = searcher.search(query)

            for result in results:
                yield result

    def get_submissions_count(self) -> int:
        return self._whoosh_search_index.doc_count()


class WhooshSearchClientFactory:
    @classmethod
    def get_instance(cls, search_index_path: Path) -> WhooshSearchClient:
        return WhooshSearchClient(cls._get_search_index(search_index_path))

    @classmethod
    def _get_search_index(cls, search_index_path: Path) -> Index:
        if os.path.exists(str(search_index_path)):
            return cls._load_search_index(search_index_path)

        return cls._create_search_index(search_index_path)

    @classmethod
    def _load_search_index(cls, search_index_path: Path) -> Index:
        return open_dir(str(search_index_path))

    @classmethod
    def _create_search_index(cls, search_index_path: Path) -> Index:
        ngram_analyzer = StandardAnalyzer() | NgramFilter(minsize=3, maxsize=4)

        os.makedirs(str(search_index_path))
        schema = fields.Schema(
            fullname=fields.TEXT(),
            title=fields.TEXT(stored=True, analyzer=ngram_analyzer),
            url=fields.TEXT(stored=True)
        )

        return create_in(str(search_index_path), schema)
